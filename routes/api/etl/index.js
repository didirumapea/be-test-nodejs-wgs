const router = require('express').Router();

// init controller
const {
    etlRestaurantnMenu,
    etlUser,
} = require('~/src/controllers/etl/etlController');

// etl restaurant & menu
router.route('/resto-and-menu')
    .get(etlRestaurantnMenu);

// etl restaurant & menu
router.route('/user-and-purchase-history')
    .get(etlUser);

module.exports = router;