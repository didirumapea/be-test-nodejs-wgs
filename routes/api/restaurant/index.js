const router = require('express').Router();

// init controller
const {
    GetRestaurant,
    GetTopRestaurant,
    GetSearchRestaurant,
} = require('~/src/controllers/restaurant/restaurantController');

// get restaurant
router.route('/')
    .get(GetRestaurant);

router.route('/top')
    .get(GetTopRestaurant);

router.route('/search')
    .get(GetSearchRestaurant);


module.exports = router;