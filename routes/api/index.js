const express = require('express');
const router = express.Router();

// PATH FOLDER
// RESTAURANT
const restaurant = require('./restaurant');

// ETL
const etl = require('./etl');

// PATH URL / API
// region RESTAURANT
router.use(`/restaurant`, restaurant);
// endregion

// region ETL
router.use(`/etl`, etl);
// endregion

module.exports = router;