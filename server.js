const dotenv = require('dotenv');
require('module-alias/register');
dotenv.config();

const http = require('http');
const https = require('https');
const fs = require('fs');
const cookieParser = require('cookie-parser');
require("@babel/register")({
    presets: ["@babel/preset-env"]
});
require("regenerator-runtime/runtime");
const { app } = require("./src/config/app");

// DATABASE CONNECTION
// connectionDB();

// START SERVER
const options = {
    key: fs.readFileSync('./ssl/key.pem'),
    cert: fs.readFileSync('./ssl/cert.pem')
}
const httpServer = http.createServer(app);
httpServer.listen(process.env.HTTP_PORT, () => {
    console.log(`app listening on port ${process.env.HTTP_PORT}`)
});
const httpsServer = https.createServer(options, app);
httpsServer.listen(process.env.HTTPS_PORT, () => {
    console.log(`app listening on port ${process.env.HTTPS_PORT}`)
});
