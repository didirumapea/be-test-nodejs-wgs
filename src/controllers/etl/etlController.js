const { errorHandler } = require('~/src/config/errorHandler');
const jsonfile = require('jsonfile');
const { db } = require('~/src/databases'); // as const knex = require('knex')(config);
const moment = require('moment');

// custom etl restaurant & menu
exports.etlRestaurantnMenu = async (req, res) => {
    try {
        // const file = 'public/vendor/wgs/big-restaurant.json';
        const file = 'public/vendor/wgs/big-restaurant.json';
        const obj = jsonfile.readFileSync(file);

        const qRestaurant = [];
        const qMenu = [];

        // Loop Restaurant
        obj.forEach((data, index) => {
            const dataRestaurant = {
                id: index + 1,
                restaurantName: data.restaurantName,
                cashBalance: data.cashBalance,
                openingHours: data.openingHours,
            };
            // Loop Menu
            data.menu.forEach(menu => {
                const dataMenu = {
                    restaurant_id: dataRestaurant.id,
                    dishName: menu.dishName,
                    price: menu.price
                };
                qMenu.push(dataMenu);
            });
            qRestaurant.push(dataRestaurant);
        });

        await db('restaurant').insert(qRestaurant); // insert ETL restaurant
        await db('menu').insert(qMenu); // insert ETL menu

        res.json({
            total: obj.length,
            success: 1,
            msg: 'ETL Restaurant & Menu success',
        });

    } catch (err) {
        errorHandler.UnHandler(res, err);
    }
}

// custom etl user & purchase history
exports.etlUser = async (req, res) => {
    try {
        const file = 'public/vendor/wgs/big-user.json';
        const obj = jsonfile.readFileSync(file);
        const qUser = [];
        const qHistory = [];

        // Loop Users
        obj.forEach((data, index) => {
            const dataUser = {
                id: index + 1,
                name: data.name,
                cashBalance: data.cashBalance,
            };

            // Loop History
            data.purchaseHistory.forEach(history => {
                const dataHistory = {
                    user_id: dataUser.id,
                    restaurantName: history.restaurantName,
                    dishName: history.dishName,
                    transactionAmount: history.transactionAmount,
                    transactionDate: moment(new Date(history.transactionDate)).format(),
                };
                qHistory.push(dataHistory);
            });
            qUser.push(dataUser);
        });

        await db('users').insert(qUser); // insert ETL users
        await db('purchase_history').insert(qHistory); // insert ETL purchase history

        res.json({
            total: obj.length,
            success: 1,
            msg: 'ETL Users & purchase history success',
        });

    } catch (err) {
        errorHandler.UnHandler(res, err);
    }
}