const { errorHandler } = require('~/src/config/errorHandler');
const { db } = require('~/src/databases'); // as const knex = require('knex')(config);
const moment = require('moment');

// get restaurant by certain time
exports.GetRestaurant = async (req, res) => {
    try {
        db.select(
            '*'
        )
            .from('restaurant')
            .whereRaw(`openingHours like ?`, [`%${ weekdays(moment(req.query.date).format('dddd')) }%`]) // Need more improvement
            .then(data => {
                res.json({
                    success: 1,
                    total: data.length,
                    msg: 'Get restaurant success',
                    result: data
                });
            })
    } catch (err) {
        errorHandler.UnHandler(res, err);
    }
}

// get top restaurant that have more or less
exports.GetTopRestaurant = async (req, res) => {
    try {
        await db.select(
            't1.id',
            't1.restaurantName',
            't1.cashBalance',
            't1.openingHours',
        )
            .from('restaurant as t1')
            .then(async resto => {
                for await (const data of resto) {
                    await db.select(
                        'dishName',
                        'price',
                    )
                        .from('menu')
                        .where('restaurant_id', data.id)
                        .then(menu => {
                            data.menu_total = menu.length;
                            data.menu = menu
                        })
                }
                // lower to high sorter default asc
                const sorted = resto.sort(function (one, other) {
                    return one.menu.length - other.menu.length
                });

                if (req.query.sort === 'desc') {
                    sorted.reverse()
                }

                res.json({
                    success: 1,
                    total: resto.length,
                    msg: 'Get restaurant success',
                    result: sorted
                });
            })
    } catch (err) {
        errorHandler.UnHandler(res, err);
    }
}

// get search restaurant
exports.GetSearchRestaurant = async (req, res) => {
    try {
        await db.select(
            't1.id',
            't1.restaurantName',
            't1.cashBalance',
            't1.openingHours',
        )
            .from('restaurant as t1')
            .whereRaw(`restaurantName like ?`, [`%${ req.query.name }%`]) // Need more improvement
            .then(async resto => {
                for await (const data of resto) {
                    await db.select(
                        'dishName',
                        'price',
                    )
                        .from('menu')
                        .where('restaurant_id', data.id)
                        .then(menu => {
                            data.menu_total = menu.length;
                            data.menu = menu
                        })
                }
                res.json({
                    success: 1,
                    total: resto.length,
                    msg: 'Get restaurant success',
                    result: resto
                });
            })
    } catch (err) {
        errorHandler.UnHandler(res, err);
    }
}

// external function

function weekdays(day) {
    switch(day) {
        case 'Sunday':
            return 'Sun';
        case 'Monday':
            return 'Mon';
        case 'Tuesday':
            return 'Tues';
        case 'Wednesday':
            return 'Weds';
        case 'Thursday':
            return 'Thurs';
        case 'Friday':
            return 'Fri';
        case 'Saturday':
            return 'Sat'
    }
}
