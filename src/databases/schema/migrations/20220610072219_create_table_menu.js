/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
    return knex.schema.createTable('menu', (table) => {
        // increment is auto added unsigned
        table.increments()
        table.integer('restaurant_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable('restaurant')
            .onDelete('CASCADE')
            .index();
        table.text('dishName');
        table.decimal('price');
        table.specificType('is_deleted', 'tinyint(1)')
            .defaultTo(0);
        table.dateTime('created_at').notNullable().defaultTo(knex.raw('CURRENT_TIMESTAMP'));
        table.dateTime('updated_at').defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'));
    })
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
    return knex.schema.dropTable('menu')
};
