/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
    return knex.schema.createTable('purchase_history', (table) => {
        // increment is auto added unsigned
        table.increments();
        table.integer('user_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable('users')
            .onDelete('CASCADE')
            .index();
        table.string('restaurantName');
        table.text('dishName');
        table.decimal('transactionAmount');
        table.datetime('transactionDate');
        table.specificType('is_deleted', 'tinyint(1)')
            .defaultTo(0);
        table.specificType('is_active', 'tinyint(1)')
            .defaultTo(0);
        table.dateTime('created_at').notNullable().defaultTo(knex.raw('CURRENT_TIMESTAMP'));
        table.dateTime('updated_at').defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'));
    })
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
    return knex.schema.dropTable('purchase_history')
};
