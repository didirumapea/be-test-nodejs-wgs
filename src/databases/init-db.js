const Knex = require('knex');

// You can dynamically pass the database name
// as a command-line argument, or obtain it from
// a .env file
const databaseName = 'db-test-wgs'; // set up your db name

const connection = {
    host: 'localhost', // set up with your host
    user: 'root', // set up with your user
    password: 'dbmember2018' // set up with your password
}

async function main() {
    let knex = Knex({
        client: 'mysql',
        connection
    })

    // Lets create our database if it does not exist
    await knex.raw('CREATE DATABASE IF NOT EXISTS ??', databaseName)


    // Now that our database is known, let's create another knex object
    // with database name specified so that we can run our migrations
    Knex({
        client: 'mysql',
        connection: {
            ...connection,
            database: databaseName,
        }
    })
    console.log('Create database done!')
    // Now we can happily run our migrations
    // await knex.migrate.latest()

    // Done!!
}

main().catch(console.log).then(process.exit)