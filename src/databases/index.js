let moment = require('moment');
moment.updateLocale(moment.locale(), { invalidDate: '-' })
let connConfig = {
    host : 'localhost',
    user : 'root',
    password : 'dbmember2018',
    database : 'db-test-wgs',
    typeCast: function (field, next) {
        if (field.type === 'DATETIME' || field.type === 'DATE') {
            return moment(field.string()).format()
        }
        return next();
    }
}
let db = require('knex')({
    client: 'mysql',
    connection: connConfig,
    useNullAsDefault: true,
    pool: {
        min: 0,
        max: 7,
    }
});
const { attachPaginate } = require('knex-paginate');
attachPaginate();
db
    .select()
    .from('knex_migrations_lock')
    .limit(1)
    .then(() => {
        console.log('connected to db')
    })
    .catch((error) => {
        console.log('db failed to connect or something happen, please check ur db.');
        console.log(error)
    });
// console.log(rows);
module.exports = {
    db
};