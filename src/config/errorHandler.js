const errorHandler = {};
errorHandler.BadRequest = (res, err) => {
    res.status(400).json({ success: 0, message: err });
};
errorHandler.Unauthorized = (res, err) => {
    res.status(401).json({ success: 0, message: err || 'Unauthorized' });
};
errorHandler.NotFound = (res, err) => {
    res.status(404).json({ success: 0, message: err });
};
errorHandler.UnHandler = (res, err) => {
    if (err.name === 'ValidationError') {
        let errorMessage;
        if (err.details) {
            errorMessage = err.details[0].message;
        } else {
            errorMessage = err.message;
        }

        res.status(400).json({ success: 0, message: errorMessage });
    } else if (err.code === 'ENOENT') {
        res.status(400).json({ success: 0, message: err.toString() });
    } else {
        const message = err.toString() || 'Something technically wrong';
        if (err.kind == 'ObjectId') {
            res.status(400).json({ success: 0, message });
        } else {
            // sendErrorLog(res, err);
            res.status(500).json({ success: 0, message });
        }
    }
};

module.exports = {
    errorHandler
}