# README #

### Step to install App ###

Clone from github
- git clone https://gitlab.com/didirumapea/be-test-nodejs-wgs.git
- cd be-test-nodejs-wgs

Install dependencies 
- npm install

Init & create database
- text "nano /src/databases/init-db.js" 
- edit host username and password provide your privileges
- text "node /src/databases/init-db.js"
- text "nano knexfile.js" 
- choose development and edit username & password provide your auth privileges

Migration database
- text "knex migrate:latest"

Start node 
- text "node server.js / nodemon / pm2 start server.js"


### ETL DATA (before start API) ###

Restaurant & Menu
- {{url_path}}etl/resto-and-menu

User & Purchase History
- {{url_path}}etl/user-and-purchase-history

Done (start the another API)